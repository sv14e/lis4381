> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Sarah Voelker

### Assignment 4 Requirements:

*Sub-Heading:*

1. Create Petstore web app
2. Provide screenshots of completed app

#### README.md file should include the following items:

* Screenshot of main screen
* Screenshot of failed validation
* Screenshot of passed validation
* Link to localhost


#### Assignment Screenshots:

*Screenshot of main screen*:

![Main Screen](img/main.png)

*Screenshot of failed validation*:

![Failed Valitdation](img/fail.png)

*Screenshot of passed validation*:
![Passed Validation](img/pass.png)

#### Assignment Links:

*localhost*:
[localhost](http://localhost/)
