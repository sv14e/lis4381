<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 11-02-17, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="LIS4381 Fall 2017">
	<meta name="author" content="Sarah Voelker">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment3</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">

					<div class="page-header">
						<?php include_once("global/header.php"); ?>
					</div>
					<?php include_once "global/footer.php"; ?>

		</div> <!-- end starter-template -->
 </div> <!-- end container -->

<h2>Assignment 3 Requirements:</h2>

<h3>Sub-Heading:</h3>

<li>Create My Events Android App</li>
<li>Provide screenshots of completed app</li>
<li>Create petstore database</li>

<h3>README.md file should include the following items:</h3>

<ul>Screenshot of running application's first user interface</ul>
<ul>Screenshot of running application's second user interface</ul>
<ul>Screenshot of ERD</ul>
<ul>Links to following files: a3.mwb and a3.sql</ul>


<h3>Assignment Screenshots:</h3>

<p>Screenshot of first user interface:</p>
<p><a href = "img/first.png">First User Interface</a></p>

<p>Screenshot of second user interface:</p>
<p><a href = "img/second.png">Second User Interface</a></p>

<p>Screenshot of ERD:</p>
<p><a href = "img/a3.png">ERD</a></p>

<h3>Assignment Links:</h3>

<p><a href = "docs/a3.mwb">mwb</a></p>
<p><a href = "docs.sql">sql</a></p>

</body>
</html>
