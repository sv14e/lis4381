> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Sarah Voelker

### Assignment 3 Requirements:

*Sub-Heading:*

1. Create My Events Android App
2. Provide screenshots of completed app
3. Create petstore database

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshot of ERD
* Links to following files: a3.mwb and a3.sql


#### Assignment Screenshots:

*Screenshot of first user interface*:

![First User Interface](img/first.png)

*Screenshot of second user interface*:

![Second User Interface](img/second.png)

*Screenshot of ERD*:
![ERD](img/a3.png)

#### Assignment Links:

*a3.mwb*:
[mwb](docs/a3.mwb)

*a3.sql*:
[sql](docs/a3.sql)
