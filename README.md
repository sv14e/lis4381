> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>

# LIS4381 - Mobile Web Application Development

## Sarah Voelker

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "A1 README")

  * Install AMPPS
  * Install JDK
  * Install Android Studio and create My First App
  * Provide screenshots of installations
  * Create Bitbucket repo
  * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
  * Provide git command descriptions

2. [A2 README.md](a2/README.md "A2 README")

  * Create Healthy Recipes Android App
  * Provide screenshots of completed app

3. [A3 README.md](a3/README.md "A3 README")

  * Create ERD based upon business rules
  * Provide screenshot of completed ERD
  * Provide DB resource links

4. [P1 README.md](p1/README.md "P1 README")

  * Create My Business Card app
  * Provide screenshots of completed app

5. [A4 README.md](a4/README.md "A4 README")

  * Create Petstore web app
  * Provide screenshots of completed app

6. [A5 README.md](a5/README.md "A5 README")

  * Create Petstore web app
  * Provide screenshots of completed app

7. [P2 README.md](p2/README.md "P2 README")

  * Create Petstore web app
  * Provide screenshots of completed app
  * Add delete/edit functionality
  * Link to RSS feed