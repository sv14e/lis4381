> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
<<<<<<< HEAD
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Sarah Voelker

### Assignment Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](http://localhost:8080 "PHP Localhost")
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* git commands with short descriptions

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
#### Git commands with short descriptions:

1. git init - initializes a git repository
2. git status - lists all files in the index instead of just the working directory
3. git add - adds file changes in your working directory to your index
4. git commit - takes all of the changes written in the index, creates a new commit object pointing to it and sets the branch to point to that new commit
5. git push - pushes all the modified local objects to the remote repository and advances its branches
6. git pull - fetches the files from the remote repository and merges it with your local one
7. git rm - removes files from your index and your working directory so they will not be tracked

#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation](http://localhost:8080 "PHP Localhost")*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/MyFirstApp.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/sv14e/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/sv14e/myteamquotes "My Team Quotes Tutorial")
