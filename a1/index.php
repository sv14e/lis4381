<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 11-02-17, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="LIS4381 Fall 2017">
	<meta name="author" content="Sarah Voelker">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 1</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">

					<div class="page-header">
						<?php include_once("global/header.php"); ?>
					</div>
					<?php include_once "global/footer.php"; ?>

		</div> <!-- end starter-template -->
 </div> <!-- end container -->

<h2>Assignment 1 Requirements:</h2>

<h3>Sub-Heading:</h3>

<li>Screenshot of AMPPS Installation</li>
<li>Screenshot of running java Hello</li>
<li>Screenshot of running Android Studio - My First App</li>
<li>git commands with short descriptions</li>

<h3>Git commands with short descriptions:</h3>
<li>git init - initializes a git repository</li>
<li>git status - lists all files in the index instead of just the working directory</li>
<li>git add - adds file changes in your working directory to your indexy</li>
<li>it commit - takes all of the changes written in the index, creates a new commit object pointing to it and sets the branch to point to that new commititory</li>
<li>git push - pushes all the modified local objects to the remote repository and advances its branches</li>
<li>git pull - fetches the files from the remote repository and merges it with your local one</li>
<li>git rm - removes files from your index and your working directory so they will not be tracked</li>

<h3>Assignment Screenshots:</h3>

<p>Screenshot of AMPPS running:</p>

<p><a href = "img/ampps.png">Ampps installation screenshot</a></p>

<p>Screenshot of running java Hello:</p>

<p><a href = "img/java.png">Hello World</a></p>

<p>Screenshot of Android Studio - My First App:</p>
<p><a href = "img/MyFirstApp.png">My First App</a></p>

</body>
</html>
