> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Sarah Voelker

### Assignment 2 Requirements:

*Sub-Heading:*

1. Create Healthy Recipes Android App
2. Provide screenshots of completed app

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface


#### Assignment Screenshots:

*Screenshot of first user interface*:

![First User Interface](img/first.png)

*Screenshot of second user interface*:

![Second User Interface](img/second.png)
