> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Sarah Voelker

### Project 2 Requirements:

*Sub-Heading:*

1. Create Petstore web app
2. Provide screenshots of completed app
3. Add delete/edit functionality
4. Link to RSS feed

#### README.md file should include the following items:

* Screenshot of index
* Screenshot of error
* Screenshot of edit page
* Screenshot of home page
* Screenshot of RSS feed
* Link to localhost


#### Assignment Screenshots:

*Screenshot of index*:

![Index](img/index.png)

*Screenshot of error*:

![Error](img/error.png)

*Screenshot of edit page*:

![Edit](img/edit.png)

*Screenshot of home page*:

![Home](img/home.png)

*Screenshot of RSS Feed*:

![RSS](img/rss.png)

#### Assignment Links:

*localhost*:
[localhost](http://localhost/)
